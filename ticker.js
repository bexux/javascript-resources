/*
    Nested data structure - Arrays of Arrays
    Flatten this data structure to work with it

    Print all of the stocks in all of the Arrays

    Use concatAll - does not already exist on the array, so need to add it
*/

// Run it on a nested array, will create new array of flattened results
// Only concats by 1 dimension, so nested 2 deep will return 1 deep.
Array.prototype.concatAll = function () {
    this.forEach(function (subArray) {
        subArray.forEach(function (item) {
            results.push(item);
        });
    });

    return results;
}

var exchanges = [
    [
        { symbol: "XFX", price: 240.22, volume: 23432 },
        { symbol: "TNZ", price: 332.19, volume: 234 }
    ],
    [
        { symbol: "JXJ", price: 120.22, volume: 534 },
        { symbol: "NYN", price: 88.47, volume: 98275 }
    ]
];

var stocks = exchanges.concatAll();

stocks.forEach(function (stock) {
    console.log(JSON.stringify(stock);
});
/*
    {"symbol":"XFX","price":240.22,"volume":23432}
    {"symbol":"TNZ","price":332.19,"volume":234}
    {"symbol":"JXJ","price":120.22,"volume":534}
    {"symbol":"NYN","price":88.47,"volume":98275}
*/