# JavaScript Resources #

This is a collection of JavaScript resources and exercises

### Async and RxJS
* [Asynchronous Programming](https://egghead.io/courses/mastering-asynchronous-programming-the-end-of-the-loop)
* [The Need to Go Reactive](https://github.com/Reactive-Extensions/RxJS#the-need-to-go-reactive)